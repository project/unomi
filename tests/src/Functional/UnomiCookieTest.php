<?php

namespace Drupal\Tests\unomi\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\unomi\UnomiCookieManager;

/**
 * Tests the Unomi Cookie.
 *
 * @group unomi
 */
class UnomiCookieTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'unomi',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer content',
      'administer unomi',
    ], parent::getAdministratorPermissions());
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

  }

  /**
   * Tests setting Unomi Cookie and get cookie value from Cookie Manager.
   */
  public function testUnomiCookie() {
    // User with required permissions
    $account = $this->drupalCreateUser(['administer unomi']);
    $this->drupalLogin($account);

    // Page is available
    $this->drupalGet('admin/config/services/unomi');
    $this->assertSession()->statusCodeEquals(200);

    // Check default cookie
    $cookieManager = new UnomiCookieManager(\Drupal::service('config.factory'), \Drupal::service('request_stack'));
    $this->assertEquals($cookieManager->getCookieName(), 'DS_VARY_ML');

    // Check If field exsists
    $this->assertSession()->pageTextContains("Cookie Name");
    $this->assertSession()->pageTextContains("Name of the cookie to be used to get segments");

    // Check default value
    $this->assertSession()->fieldValueEquals('edit-cookie-name', 'DS_VARY_ML');

    $edit = [
      'edit-cookie-name' => 'Changed_Cookie_Name',
    ];

    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextNotContains("The configuration options have been saved.");

    $edit = [
      'edit-cookie-name' => 'Changed_Cookie_Name',
      'edit-connector-config-host' => 'HostName',
      'edit-connector-config-port' => '443',
      'edit-connector-config-auth-username' => 'SampleUsername',
    ];

    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains("The configuration options have been saved.");

    $cookieManager = new UnomiCookieManager(\Drupal::service('config.factory'), \Drupal::service('request_stack'));
    $this->assertEquals($cookieManager->getCookieName(), 'Changed_Cookie_Name');

    $edit = [
      'edit-cookie-name' => '',
    ];

    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextNotContains("The configuration options have been saved");

    $edit = [
      'edit-cookie-name' => 'changedCookie',
    ];

    $this->submitForm($edit, 'Save configuration');
    $cookieManager = new UnomiCookieManager(\Drupal::service('config.factory'), \Drupal::service('request_stack'));
    $this->assertEquals($cookieManager->getCookieName(), 'changedCookie');

  }

}
