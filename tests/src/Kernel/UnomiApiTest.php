<?php

namespace Drupal\Tests\unomi\Kernel;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Config;
use Drupal\unomi\Plugin\UnomiConnector\BasicAuthUnomiConnector;
use Drupal\unomi\UnomiConnector\UnomiConnectorPluginManager;
use Drupal\unomi\UnomiApi;

/**
 * Test UnomiApi.
 *
 * @coversDefaultClass \Drupal\unomi\UnomiApi
 * @group unomi
 */
class UnomiApiTest extends EntityKernelTestBase {
  use AssertMailTrait;
  use StringTranslationTrait;

  /**
   * The Unomi Api.
   *
   * @var \Drupal\unomi\UnomiApi
   */
  protected $unomiApi;

  /**
   * Connector ID.
   *
   * @var string
   */
  protected $connectorId;

  /**
   * Connector Config.
   *
   * @var string
   */
  protected $connectorConfig;

  /**
   * {@inheritDoc}.
   */
  protected static $modules = [
    'unomi',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->installConfig(['unomi']);
    $this->installConfig('unomi', ['plugin.plugin_configuration.unomi_connector.standard']);
    $this->installConfig('unomi', ['plugin.plugin_configuration.unomi_connector.basic_auth']);
    // Create mock to return config that will be used in the code under test.
    $this->configMock = $this->prophesize(Config::class);

    $this->connector_id = 'basic_auth';
    $this->connector_fail_id = 'fail';
    $this->connector_config = [
      'scheme' => 'https',
      'host' => 'hostName.com',
      'port' => '443',
      'path' => '/',
      'timeout' => '5',
      'username' => 'username',
      'password' => 'password',
    ];
    $this->configMock->get('connector')->willReturn($this->connector_id);
    $this->configMock->get('connector_config')->willReturn($this->connector_config);

    $this->configFactoryMock = $this->prophesize(ConfigFactoryInterface::class);
    $this->configFactoryMock->get('unomi.settings')->willReturn($this->configMock);

    // Mock Basic auth connector and Unomi connector plugin.
    $mock_builder = $this->getMockBuilder(BasicAuthUnomiConnector::class)
      ->disableOriginalConstructor();
    $pluginMock = $mock_builder->getMock();
    $pluginMock->expects($this->any())
      ->method('getClient')
      ->with()
      ->willReturn(new Segments());
    $pluginMock->expects($this->any())
      ->method('getApiClient')
      ->with()
      ->willReturn(new Segments());

    $mock_builder = $this->getMockBuilder(UnomiConnectorPluginManager::class)
      ->disableOriginalConstructor();
    $unomiPluginMock = $mock_builder->getMock();
    $unomiPluginMock->expects($this->any())
      ->method('createInstance')
      ->with($this->connector_id, $this->connector_config)
      ->willReturn($pluginMock);
    $unomiPluginMock->expects($this->any())
      ->method('hasDefinition')
      ->with($this->connector_id)
      ->willReturn(
        TRUE
      );
    $unomiPluginMockFail = $mock_builder->getMock();
    $unomiPluginMockFail->expects($this->any())
      ->method('createInstance')
      ->with($this->connector_id, $this->connector_config)
      ->willReturn($pluginMock);
    $unomiPluginMockFail->expects($this->any())
      ->method('hasDefinition')
      ->with($this->connector_id)
      ->willReturn(
        FALSE
      );

    $this->unomiApiPass = new UnomiApi($this->configFactoryMock->reveal(), \Drupal::service('state'), \Drupal::service('url_generator'), \Drupal::service('request_stack'), \Drupal::messenger(), $unomiPluginMock, \Drupal::logger('unomi'), \Drupal::service('cache.unomi'));
    $this->unomiApiPassFail = new UnomiApi($this->configFactoryMock->reveal(), \Drupal::service('state'), \Drupal::service('url_generator'), \Drupal::service('request_stack'), \Drupal::messenger(), $unomiPluginMockFail, \Drupal::logger('unomi'), \Drupal::service('cache.unomi'));

  }

  /**
   * Test for getClient().
   *
   * @group unomi
   * @cover ::getClient
   */
  public function testClient() {
    $this->assertEquals(TRUE, is_object($this->unomiApiPass->getClient()));
    $this->assertEquals(FALSE, is_object($this->unomiApiPassFail->getApiClient()));
  }

  /**
   * Test for getApiClient().
   *
   * @group unomi
   * @cover ::getApiClient
   */
  public function testApiClient() {
    $this->assertEquals(TRUE, is_object($this->unomiApiPass->getApiClient()));
    $this->assertEquals(FALSE, is_object($this->unomiApiPassFail->getApiClient()));
  }

  /**
   * Test for getSegments().
   *
   * @group unomi
   * @cover ::getSegments
   */
  public function testSegments() {
    $segmentList = [
      'id1' => 'value1',
    ];
    $emptySegments = [
      '_unknown' => $this->t('Unknown segment'),
    ];

    $this->assertEquals($emptySegments, $this->unomiApiPass->getSegments(TRUE));
    $cid = 'unomi:segments';
    $cache = \Drupal::service('cache.unomi');
    $cache->set($cid, $segmentList, CacheBackendInterface::CACHE_PERMANENT, ['unomi:segments']);
    $this->assertEquals($segmentList, $this->unomiApiPass->getSegments(TRUE));
    $cache->delete($cid);
    $this->assertEquals($emptySegments, $this->unomiApiPass->getSegments(TRUE));
  }

}

/**
 * Mock Class for listing segments.
 */
class ListSegments {

  /**
   * Mock function for list segements.
   */
  public function listSegments() {
    return TRUE;
  }

}

/**
 * Mock Class for segments.
 */
class Segments {

  /**
   * Mock function for segements.
   */
  public function segments() {
    return new ListSegments();
  }

}
