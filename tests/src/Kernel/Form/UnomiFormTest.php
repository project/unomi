<?php

namespace Drupal\Tests\unomi\Kernel\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormState;
use Drupal\unomi\Form\UnomiSettingsForm;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\unomi\Plugin\UnomiConnector\BasicAuthUnomiConnector;
use Drupal\unomi\UnomiConnector\UnomiConnectorPluginManager;

/**
 * Test class for UnomiSettingsForm.
 * 
 * @coversDefaultClass \Drupal\unomi\Form\UnomiSettingsForm
 * @group unomi
 */
class UnomiFormTest extends KernelTestBase {

  use StringTranslationTrait;

  /**
   * Config Factory Mock.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactoryMock;

  /**
   * Config Mock.
   *
   * @var \Drupal\Core\Config\Config
   */
  private $configMock;

  /**
   * Unomi Settings Form.
   *
   * @var \Drupal\unomi\Form\UnomiSettingsForm
   */
  private $form;

  /**
   * {@inheritDoc}.
   */
  protected static $modules = [
    'unomi',
  ];

  /**
   * {@inheritDoc}.
   */
  protected function setUp(): void {
    parent::setUp();

    // Create mock to return config that will be used in the code under test.
    $this->configMock = $this->prophesize(Config::class);

    $this->connector_id = 'basic_auth';
    $this->connector_config = [
      'scheme' => 'https',
      'host' => 'hostName.com',
      'port' => '443',
      'path' => '/',
      'timeout' => '5',
      'username' => 'username',
      'password' => 'password',
    ];
    $this->cookie_name = "DS_VARY_ML";
    $this->configMock->get('connector')->willReturn($this->connector_id);
    $this->configMock->get('cookie_name')->willReturn($this->cookie_name);
    $this->configMock->get('connector_config')->willReturn($this->connector_config);

    $this->configFactoryMock = $this->prophesize(ConfigFactoryInterface::class);
    $this->configFactoryMock->getEditable('unomi.settings')->willReturn($this->configMock);
  }

  /**
   * Test for correct connector credentials.
   */
  public function testSuccessConnection() {

    // Mock Basic auth connector and Unomi connector plugin.
    $mock_builder = $this->getMockBuilder(BasicAuthUnomiConnector::class)
      ->disableOriginalConstructor();
    $pluginMock = $mock_builder->getMock();
    $pluginMock->expects($this->any())
      ->method('getClient')
      ->with()
      ->willReturn(new Segments());

    $mock_builder = $this->getMockBuilder(UnomiConnectorPluginManager::class)
      ->disableOriginalConstructor();
    $unomiPluginMock = $mock_builder->getMock();
    $unomiPluginMock->expects($this->any())
      ->method('createInstance')
      ->with($this->connector_id, $this->connector_config)
      ->willReturn($pluginMock);
    $unomiPluginMock->expects($this->any())
      ->method('getDefinitions')
      ->with()
      ->willReturn([
        'id1' => [
          'label' => 'basic_auth',
        ],
      ]);

    // Creating form with a connection with a purpose to pass.
    $this->form = new UnomiSettingsForm($this->configFactoryMock->reveal(), \Drupal::service('unomi_api'), $unomiPluginMock, \Drupal::messenger());

    $form = [];
    $form_state = new FormState();

    // Build the form.
    $retForm = $this->form->buildForm($form, $form_state);
    // Expected result.
    $expected = [
      '#title' => $this->t('Connection Status'),
      '#open' => TRUE,
      '#description' => $this->t('Connection successfully established via basic_auth!'),
      '#weight' => -1,
      '#type' => 'fieldset',
    ];
    $this->assertEquals($expected, $retForm['status']);

  }

  /**
   * Test for wrong connector credentials.
   */
  public function testFailConnection() {
    // Creating form with a connection with a view to fail.
    $this->form = new UnomiSettingsForm($this->configFactoryMock->reveal(), \Drupal::service('unomi_api'), \Drupal::service('plugin.manager.unomi.connector'), \Drupal::messenger());

    $form = [];
    $form_state = new FormState();

    // Build the form.
    $retForm = $this->form->buildForm($form, $form_state);
    // Expected result.
    $expected = [
      '#title' => $this->t('Connection Status'),
      '#open' => TRUE,
      '#description' => $this->t('Connection failed, there was a problem with the connection to the UNOMI API instance.'),
      '#weight' => -1,
      '#type' => 'fieldset',
    ];
    $this->assertEquals($expected, $retForm['status']);
  }

}

/**
 * Mock Class for listing segments.
 */
class ListSegments {

  /**
   * Mock function for list segements.
   */
  public function listSegments() {
    return TRUE;
  }

}

/**
 * Mock Class for segments.
 */
class Segments {

  /**
   * Mock function for segements.
   */
  public function segments() {
    return new ListSegments();
  }

}
